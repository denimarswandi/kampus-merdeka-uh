const db = require('../models');

const Todo = {
    getAll: async (req, res) => {
        try {
            const todo = await db.Todo.findAll();
            return res.send(todo)
        } catch (e) {
            console.log(e)
        }
    },
    findOne: async (req, res) => {
        const todo = await db.Todo.findOne({
            where: {
                id: req.params.id
            }
        })
        if (!todo) {
            return res.status(400).send({ msg: 'not found' })
        }
        return res.send(todo)
    },
    create: async (req, res) => {
        try {
            await db.Todo.create({ text: req.body.text })
            console.log(req.body.text)
            return res.send({ msg: 'created' })
        } catch (err) {
            console.log(req.body.text)
            res.status(500).send({msg:'uuups'})
        }
    },
    update: async(req, res)=>{
        await db.Todo.update({
            text:req.body.text
        },{
            where: {id:req.params.id}
        })
        return res.send({msg:'updated'})
    },
    delete: async(req, res)=>{
        await db.Todo.destroy({where:{id:req.params.id}})
        return res.send({msg:'deleted'})
    }
}

module.exports = Todo;