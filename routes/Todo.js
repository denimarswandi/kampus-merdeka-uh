const express = require('express');
const todo = require('../controller/Todo');

const router = express.Router();

router.get('/', todo.getAll);
router.post('/create', todo.create)
router.get('/todo/:id', todo.findOne)
router.put('/todo/:id', todo.update)
router.delete('/todo/:id', todo.delete)

module.exports = router;