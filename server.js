const experss = require('express');
const db = require('./models');
const cors = require('cors');
const routerTodo = require('./routes/Todo')

const PORT = process.env.port || 3000;
const app = experss();

app.use(experss.urlencoded({extended:true}));
app.use(cors())
app.use(experss.json())

app.use('/', routerTodo)



app.listen(PORT, async ()=>{
    try{
        await db.sequelize.sync()
    }catch(err){
        console.log(err)
    }
    console.log(`port:${PORT}`)
})